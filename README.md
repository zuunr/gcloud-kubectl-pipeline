# gcloud-kubectl-pipeline

This module contains a Dockerfile to create a docker image based on the google/cloud-sdk alpine image, with the additional component kubectl installed.

It is intended to be used in Bitbucket pipelines to activate kubectl functionality.

## Supported tags

* [`3.0.0`, (*3.0.0/Dockerfile*)](https://bitbucket.org/zuunr/gcloud-kubectl-pipeline/src/3.0.0/Dockerfile)
* [`2.0.0`, (*2.0.0/Dockerfile*)](https://bitbucket.org/zuunr/gcloud-kubectl-pipeline/src/2.0.0/Dockerfile)
* [`1.0.0`, (*1.0.0/Dockerfile*)](https://bitbucket.org/zuunr/gcloud-kubectl-pipeline/src/1.0.0/Dockerfile)

## Usage

To use the image in a Bitbucket pipeline environment and authenticate with your gcloud environment, use the following yaml configuration as a base:

```yaml
- step:
    name: My kubectl step
    # Check the image version tag
    image: zuunr/gcloud-kubectl-pipeline:3.0.0
    script:
      # Set the $GCLOUD_API_KEYFILE to the contents of your gcloud service account key file in the Bitbucket pipelines environment variables
      - echo $GCLOUD_API_KEYFILE > ./gcloud-api-key.json
      - gcloud auth activate-service-account --key-file gcloud-api-key.json
      # Replace <MY_PROJECT> with your actual project
      - gcloud config set project <MY_PROJECT>
      # Replace <MY_COMPUTEZONE> with your actual compute/zone
      - gcloud config set compute/zone <MY_COMPUTEZONE>
      # Replace <MY_KUBE_CLUSTER> with your actual cluster name
      - gcloud container clusters get-credentials <MY_KUBE_CLUSTER>
      - kubectl get pods
```
